#include <iostream>
#include "Reunion.h"
using namespace std;

int main()
{
    Reunion r[6];
    Persona brian("Brian", "Lara"), angel("Angel", "Simon"), maxiW("Maxi", "Wenner"), laura("Laura", "Velez"), maxiS("Maxi", "Sar");
    r[0].setFecha(Fecha(1, 10, 2023));
    r[0].setHorario(Horario(19,0,0));
    r[0].setLugar("Buenos Aires");
    r[0].setTema("Programación en C++");
    r[0].setDuracion(90);
    Persona integrantes1[5] = {brian, angel};
    r[0].setIntegrantes(integrantes1);

    r[1].setFecha(Fecha(1, 4, 2023));
    r[1].setHorario(Horario(19,0,0));
    r[1].setLugar("Internet");
    r[1].setTema("Estadística y Programación");
    r[1].setDuracion(100);
    r[1].agregarIntegrante(maxiW);

    r[2].setFecha(Fecha(20, 11, 2023));
    r[2].setHorario(Horario(23,0,0));
    r[2].setLugar("Discord");
    r[2].setTema("Goto Gamejam 2023");
    r[2].setDuracion(0);
    r[2].agregarIntegrante(brian);

    r[3].setFecha(Fecha(1, 3, 2024));
    r[3].setHorario(Horario(18,0,0));
    r[3].setLugar("Buenos Aires");
    r[3].setTema("Bases de Datos con SQL");
    r[3].setDuracion(60);
    Persona integrantes2[5] = {laura, angel};
    r[3].setIntegrantes(integrantes2);

    r[4].setFecha(Fecha(1, 10, 2023));
    r[4].setHorario(Horario(18,0,0));
    r[4].setLugar("Discord");
    r[4].setTema("C#");
    r[4].setDuracion(50);
    r[4].agregarIntegrante(maxiS);

    r[5].setFecha(Fecha(5, 11, 2023));
    r[5].setHorario(Horario(21,0,0));
    r[5].setLugar("Steam");
    r[5].setTema("Torneo de Age of Empires");
    r[5].setDuracion(80);
    Persona integrantes3[5] = {brian, angel, maxiS, maxiW};
    r[5].setIntegrantes(integrantes3);

        int maxDur = 0;
        int pos;

    for (int i=0;i<6;i++)
    {
        if (r[i].getDuracion() > maxDur)
        {
            maxDur = r[i].getDuracion();
            pos = i;
        }
    }
    cout << "Integrantes de la reunión más larga:\n";
    for (int i=0; i < 5; i++)
    {
        if (r[pos].getIntegrantes()[i].toString() != " ") cout << r[pos].getIntegrantes()[i].toString() << endl;
    }

    cout << "\nTemas de las reuniones del 2023:\n";
    for (int i=0; i < 6; i++)
    {
        if (r[i].getFecha().getAnio() == 2023 ) cout << r[i].getTema() << endl;
    }

    cout << "\nTemas de las reuniones del 2024:\n";
    for (int i=0; i < 6; i++)
    {
        if (r[i].getFecha().getAnio() == 2024) cout << r[i].getTema() << endl;
    }

    cout << "\nCantidad de reuniones después de las 20hs:\n";
    int cant=0;
    for (int i=0; i < 6; i++)
    {
        if (r[i].getHorario().getHoras() > 20 ) cant++;
    }
    cout << cant << endl;
    return 0;
}
