#ifndef HORARIO_H
#define HORARIO_H


class Horario
{
    public:
        Horario();
        Horario(int horas, int minutos, int segundos);
        virtual ~Horario();
        int getHoras();
        int getMinutos();
        int getSegundos();
        void setHoras(int horas);
        void setMinutos(int minutos);
        void setSegundos(int segundos);
        std::string toString();
    protected:

    private:
        int _horas;
        int _minutos;
        int _segundos;
};

#endif // HORARIO_H
