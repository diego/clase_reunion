#ifndef PERSONA_H
#define PERSONA_H
#include <string>

class Persona
{
    public:
        Persona();
        Persona(std::string nombre, std::string apellido);
        virtual ~Persona();
        std::string getNombre();
        std::string getApellido();
        void setNombre(std::string nombre);
        void setApellido(std::string apellido);
        std::string toString();

    protected:

    private:
        std::string _nombre;
        std::string _apellido;
};

#endif // PERSONA_H
