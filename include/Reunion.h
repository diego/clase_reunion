#ifndef REUNION_H
#define REUNION_H

#include <cstring>

#include "Persona.h"
#include "Fecha.h"
#include "Horario.h"

class Reunion
{
    public:
        Reunion();
        virtual ~Reunion();
        void setFecha(Fecha fecha);
        void setHorario(Horario horario);
        void setLugar(std::string lugar);
        void setTema(std::string tema);
        void setDuracion(int duracion);
        void setIntegrantes(Persona *integrantes);
        std::string getLugar();
        std::string getTema();
        int getDuracion();
        void getIntegrantes(Persona *integrantes);
        Persona *getIntegrantes();
        Fecha getFecha();
        Horario getHorario();
        int agregarIntegrante(Persona p);
        Persona getIntegrante(int posicion);
        int getCantidadIntegrantes();

    protected:

    private:
        Fecha _fecha;
        Horario _horario;
        std::string _lugar, _tema;
        int _duracion=0;

        Persona _integrantes[5];
};

#endif // REUNION_H
