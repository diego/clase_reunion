#include <iostream>
using namespace std;

#include "Persona.h"

Persona::Persona()
{
    setNombre("");
    setApellido("");
}

Persona::Persona(string nombre, string apellido)
{
    setNombre(nombre);
    setApellido(apellido);
}

Persona::~Persona()
{
    //dtor
}

string Persona::getNombre()
{
    return _nombre;
}
string Persona::getApellido()
{
    return _apellido;
}
void Persona::setNombre(string nombre)
{
    if (nombre != "") _nombre = nombre;
}
void Persona::setApellido(string apellido)
{
if (apellido != "") _apellido = apellido;
}

string Persona::toString()
{
    return getNombre() + " " + getApellido();
}
