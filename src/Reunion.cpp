#include <iostream>
using namespace std;


#include "Reunion.h"

Reunion::Reunion()
{
    //ctor
}

Reunion::~Reunion()
{
    //dtor
}

void Reunion::setIntegrantes(Persona *integrantes)
{
    for (int i=0; i < 5; i++) _integrantes[i] = integrantes[i];
}
Persona *Reunion::getIntegrantes()
{
    return _integrantes;
}
void Reunion::setFecha(Fecha fecha)
{
    _fecha = fecha;
}
Fecha Reunion::getFecha()
{
    return _fecha;
}

int Reunion::agregarIntegrante(Persona p)
{
    Persona *integrantes;
    integrantes = getIntegrantes();
    for (int i=0; i < 5; i++)
    {
        if (integrantes[i].toString() == " ")
        {
            integrantes[i] = p;
            return i;
        }
    }
    return -1;
}


Persona Reunion::getIntegrante(int posicion)
{
    return getIntegrantes()[posicion];
}

int Reunion::getCantidadIntegrantes()
{
    int cant = 0;
    for (int i=0; i<5;i++)
    {
        if (getIntegrante(i).toString() == " ") return cant;
        cant++;
    }
}

Horario Reunion::getHorario(){
    return _horario;
}
void Reunion::setHorario(Horario horario)
{
    _horario = horario;
}

void Reunion::setLugar(std::string lugar)
{
    _lugar = lugar;
}
void Reunion::setTema(std::string tema)
{
    _tema = tema;
}
void Reunion::setDuracion(int duracion)
{
    _duracion = duracion;
}
std::string Reunion::getLugar()
{
    return _lugar;
}
std::string Reunion::getTema()
{
    return _tema;
}
int Reunion::getDuracion()
{
    return _duracion;
}
