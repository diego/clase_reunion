#include <iostream>
using namespace std;
#include <ctime>
#include "Horario.h"

Horario::Horario()
{
    std::time_t now = std::time(NULL);
    std::tm * local_time = std::localtime(&now);
    _horas = local_time->tm_hour;
    _minutos = local_time->tm_min;
    _segundos = local_time->tm_sec;
}

Horario::Horario(int horas, int minutos, int segundos)
{
    if (horas > 23 || horas < 0 || minutos > 59 || minutos < 0 || segundos > 59 || segundos < 0)
        {
            std::time_t now = std::time(NULL);
            std::tm * local_time = std::localtime(&now);
            _horas = local_time->tm_hour;
            _minutos = local_time->tm_min;
            _segundos = local_time->tm_sec;
            return;
        }
    _horas = horas;
    _minutos = minutos;
    _segundos = segundos;
}

Horario::~Horario()
{
    //dtor
}

int Horario::getHoras()
{
    return _horas;
}
int Horario::getMinutos()
{
    return _minutos;
}
int Horario::getSegundos()
{
    return _segundos;
}
void Horario::setHoras(int horas)
{
    _horas = horas;
}
void Horario::setMinutos(int minutos)
{
    _minutos = minutos;
}
void Horario::setSegundos(int segundos)
{
    _segundos = segundos;
}

string Horario::toString()
{
    string h = getHoras() >=10 ? to_string(getHoras()) : "0" + to_string(getHoras());
    string m = getMinutos() >=10 ? to_string(getMinutos()) : "0" + to_string(getMinutos());
    string s = getSegundos() >=10 ? to_string(getSegundos()) : "0" + to_string(getSegundos());
    return h + ":" + m + ":" + s;
}
